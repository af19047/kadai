import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class OrderFood {
    public static final int TEMPURA_VALUE=100;
    public static final int KARAAGE_VALUE=100;
    public static final int GYOUZA_VALUE=100;
    public static final int UDON_VALUE=100;
    public static final int YAKISOBA_VALUE=100;
    public static final int RAMEN_VALUE=100;
    private JPanel root;
    private JButton tempuraButton;
    private JButton karaageButton;
    private JButton gyouzaButton;
    private JButton udonButton;
    private JButton yakisobaButton;
    private JButton ramenButton;
    private JButton checkOutButton;
    private JTextPane receivedInfo;
    private JTextPane totalInfo;
    int count(String order){
        int i;
        int value=0;
        int last=0;
        String tempura="Tempura";
        String karaage="Karaage";
        String gyouza="Gyouza";
        String udon="Udon";
        String yakisoba="Yakisoba";
        String ramen="Ramen";

        do {
            i=order.indexOf("Tempura",last);
            if (i!=-1)
                value += TEMPURA_VALUE;
            last=i+tempura.length();
        }while(i!=-1);
        last=0;
        do {
            i=order.indexOf("Karaage",last);
            if (i!=-1)
                value += KARAAGE_VALUE;
            last=i+karaage.length();
        }while(i!=-1);
        last=0;
        do {
            i=order.indexOf("Gyouza",last);
            if (i!=-1)
                value += GYOUZA_VALUE;
            last=i+gyouza.length();
        }while(i!=-1);
        last=0;
        do {
            i=order.indexOf("Udon",last);
            if (i!=-1)
                value += UDON_VALUE;
            last=i+udon.length();
        }while(i!=-1);
        last=0;
        do {
            i=order.indexOf("Yakisoba",last);
            if (i!=-1)
                value += YAKISOBA_VALUE;
            last=i+yakisoba.length();
        }while(i!=-1);
        last=0;
        do {
            i=order.indexOf("Ramen",last);
            if (i!=-1)
                value += RAMEN_VALUE;
            last=i+ramen.length();
        }while(i!=-1);


        return value;
    }
    void order(String food){
        String retention=receivedInfo.getText();

        int confirmation=JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            JOptionPane.showMessageDialog(null, "Order for "+food+" received.");
            receivedInfo.setText(retention+"\n"+food);
            totalInfo.setText("Total   "+count(receivedInfo.getText())+"yen");

        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("OrderFood");
        frame.setContentPane(new OrderFood().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public OrderFood() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });
        gyouzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyouza");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String retention = receivedInfo.getText();

                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is " + count(receivedInfo.getText()) + " yen.");
                    receivedInfo.setText(null);
                    totalInfo.setText("Total   0 yen");
                }
            }
        });
    }
}
